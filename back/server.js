const Koa = require('koa'),
  Cors = require('koa2-cors'),
  http = require('http'),
  config = require('./config'),
  Router = require('koa-router'),
  Storage = require('./storage'),
  BodyParser = require('koa-bodyparser'),
  fs = require('fs'),
  path = require('path'),
  serve = require('koa-static'),
  ErrorHandler = require('./util/ErrorHandler');
app = new Koa();
app.use(Cors({
  origin: '*',
  allowMethods: ['GET', 'POST']
}));

let routs = new Router();

app.use(ErrorHandler);
app.use(BodyParser({
  jsonLimit: '56kb'
}));

console.log(__dirname);

app.use(serve(`${path.join(__dirname, 'dist')}`));

const transactionStorage = new Storage();

routs.get('/', (ctx) => {
  ctx.type = 'html';
  ctx.body = fs.createReadStream(`${path.join(__dirname, 'dist', 'index.html')}`)
}).get('/history', (ctx) => {
  ctx.status = 200;
  ctx.body = transactionStorage.storage;
}).get('/get/:id', (ctx) => {
  const {id} = ctx.params;
  ctx.body = transactionStorage.getTransactionById(id);
}).get('/balance', (ctx) => {
  ctx.body = transactionStorage.balance;
}).post('/add', (ctx) => {
  const transaction = ctx.request.body;
  ctx.body = transactionStorage.addTransaction(transaction);
});

app.use(routs.routes());

const server = http.createServer(app.callback()).listen(config.server.port, function () {
  console.log('%s listening at port %d', config.app.name, config.server.port);
});