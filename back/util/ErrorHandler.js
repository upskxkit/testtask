module.exports = async (ctx, next) => {
  try {
    await next();
  } catch (err) {

    if (err.status) {
      ctx.body = err.message;
      ctx.status = err.status;
    } else {
      ctx.body = 'Error 500';
      ctx.status = 500;
      console.error(err.message, err.stack);
    }

  }
};