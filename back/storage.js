const Exeption = require('./util/Eception');

class Storage {

  constructor() {
    this._storage = new Map();
  }

  get storage() {
    return [...this._storage.values()].sort((a, b) => {
      return a.created_at - b.created_at;
    });
  }

  getTransactionById(id) {

    const _id = parseInt(id);

    if (!this._storage.has(_id)) {
      throw new Exeption(400, 'Incorrect id of Transaction');
    }


    return this._storage.get(_id);
  }

  addTransaction(data) {
    let id = this._storage.size;

    const transaction = new Transaction(id, data.type, data.amount);
    const currentBalance = this.balance;

    if (transaction.type === TransactionType.Credit) {

      if (!this._isPositive(currentBalance, transaction.amount)) {
        transaction.status = TransactionStatus.Reject;

        this._storage.set(id, transaction);
        throw new Exeption(400, 'Transaction refused');
      }
    }

    this._storage.set(id, transaction);

    return 'transaction added successfully';
  }

  get balance() {
    return this.storage.reduce((sum, transaction) => {
      if (transaction.status === TransactionStatus.Reject) {
        return sum;
      }

      if (transaction.type === TransactionType.Debit) {
        return sum + transaction.amount;
      }

      return sum - transaction.amount;

    }, 0)
  }

  _isPositive(currentBalance, creditAmount) {

    return currentBalance - creditAmount > 0;

  }

}

class Transaction {
  constructor(id = 0, type = TransactionType.Credit, amount = 0, status = TransactionStatus.Approve) {
    this.id = id;
    this.type = type;
    this.amount = Math.abs(amount);
    this.status = status;
    this.created_at = Date.now();
  }
}

class TransactionType {
  static get Credit() {
    return 'credit';
  };

  static get Debit() {
    return 'debit';
  }

  static get All() {
    return [this.Credit, this.Debit];
  }

}

class TransactionStatus {
  static get Approve() {
    return 'approve';
  }

  static get Reject() {
    return 'reject';
  }
}

module.exports = Storage;