import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";
import {Observable} from "rxjs";

@Injectable()
export class TransactionService {

  constructor(protected http: HttpClient) {
  }

  public history(): Observable<any> {
    return this.http.get(`${environment.server_url}/history`)
  }

  public get(id: string): Observable<any> {
    return this.http.get(`${environment.server_url}/get/${id}`)
  }

  public add(transaction): Observable<any> {
    return this.http.post(`${environment.server_url}/add`, transaction);
  }

  public balance(): Observable<any> {
    return this.http.get(`${environment.server_url}/balance`)
  }

}
