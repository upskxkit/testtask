import {Component, OnInit} from '@angular/core';
import {TransactionService} from "./transaction.service";
import {Transaction, TransactionStatus, TransactionType} from "./transaction.interface";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'AgileEngine';
  public selectedItem: Transaction = null;

  constructor(private transactionService: TransactionService) {
  }

  public transactions: Transaction[] = [];

  public isReject(type): string {
    if (type === TransactionStatus.Reject) {
      return 'badge-danger'
    }
    return 'badge-success';
  }

  public isCredit(type): string {
    if (type === TransactionType.Credit) {
      return 'badge-warning'
    }
    return 'badge-primary';
  }

  public isCurrent(id: string): boolean {

    return !!this.selectedItem && this.selectedItem.id === id;

  }


  public getById(id: string) {
    this.transactionService.get(id).subscribe(data => {
      this.selectedItem = {...data};
    })
  }

  ngOnInit(): void {
    this.transactionService.history().subscribe(data => {
      this.transactions = [...data]
    });
  }
}
