export enum TransactionStatus {
  Approve = 'approve',
  Reject = 'reject'
}

export enum TransactionType {
  Credit = 'credit',
  Debit = 'debit'
}


export interface Transaction {
  status: TransactionStatus,
  amount: number,
  create_at: Date,
  type: TransactionType,
  id: string
}
